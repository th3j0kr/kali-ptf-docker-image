#!/bin/bash
echo '[*] Fixing sources.list'
if [ -f '/etc/apt/sources.list' ]; then
    mv /etc/apt/sources.list /etc/apt/sources.list.bak
fi

echo 'deb http://http.kali.org/kali kali-rolling main contrib non-free' > /etc/apt/sources.list
echo 'deb-src http://http.kali.org/kali kali-rolling main contrib non-free' >> /etc/apt/sources.list

echo '[*] Done!'