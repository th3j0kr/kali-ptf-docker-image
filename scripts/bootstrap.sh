#!/bin/bash
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# Start Install PTF tools
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
echo '[*] Installing hacking tools with ptf'

cd /pentest/ptf

#echo -en "use modules/install_update_all\nyes\n" | python ptf

echo -en "use modules/exploitation/beef\nrun\n" | python ptf
echo -en "use modules/exploitation/findsploit\nrun\n" | python ptf
echo -en "use modules/exploitation/responder\nrun\n" | python ptf
echo -en "use modules/exploitation/routersploit\nrun\n" | python ptf
echo -en "use modules/exploitation/setoolkit\nrun\n" | python ptf
echo -en "use modules/exploitation/sqlmap\nrun\n" | python ptf
echo -en "use modules/exploitation/wpsploit\nrun\n" | python ptf
echo -en "use modules/intelligence-gathering/dnsrecon\nrun\n" | python ptf
echo -en "use modules/intelligence-gathering/enum4linux\nrun\n" | python ptf
echo -en "use modules/intelligence-gathering/windows-exploit-suggester\nrun\n" | python ptf
echo -en "use modules/password-recovery/seclist\nrun\n" | python ptf
echo -en "use modules/pivoting/pivoter\nrun\n" | python ptf
echo -en "use modules/pivoting/dnscat2\nrun\n" | python ptf
echo -en "use modules/post-exploitation/crackmapexec\nrun\n" | python ptf
echo -en "use modules/post-exploitation/empire\nrun\n" | python ptf
echo -en "use modules/post-exploitation/unicorn\nrun\n" | python ptf
echo -en "use modules/post-exploitation/trevorc2\nrun\n" | python ptf
echo -en "use modules/vulnerability-analysis/nikto\nrun\n" | python ptf
echo -en "use modules/vulnerability-analysis/office365userenum\nrun\n" | python ptf
echo -en "use modules/vulnerability-analysis/sslscan\nrun\n" | python ptf
echo -en "use modules/vulnerability-analysis/wpscan\nrun\n" | python ptf
echo -en "use modules/vulnerability-analysis/faradaysec\nrun\n" | python ptf
echo -en "use modules/webshells/weevely\nrun\n" | python ptf

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# End Install PTF tools
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# Start Install Custom tools
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

mkdir /pentest/custom
cd /pentest/custom

# RDP password spray
git clone https://github.com/xFreed0m/RDPassSpray.git
cd ./RDPassSpray
pip3 install -r requirements.txt

# Seclists
cd /pentest/custom
git clone https://github.com/danielmiessler/SecLists.git


#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# End Install Custom tools
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# Start Custom scripts
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
mkdir -p /root/sbin/scripts
git clone https://gitlab.purpleteamsec.com/th3J0kr/lazy-scripts.git /root/sbin/scripts

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# End Custom scripts
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# Start Image Customization
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
cd /
# Create engagements folder
mkdir /root/engagements

# Generate an ssh key
ssh-keygen -b 2048 -t rsa -f /root/.ssh/id_rsa -q -N ""

#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# End Image Customization
#-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=