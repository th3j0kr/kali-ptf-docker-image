# The Custom Hackin' Container

## Overview

This is the source files and build scripts for my custom kali hackin' container. The container is built on top of the official kali container, then TrustedSec's [Penetration Testing Framework (PTF)](https://github.com/trustedsec/ptf) is installed which is then used to install some additional tooling. Finally some additional tools are installed by various methods to ensure an all-in-one hackin' box.

## Building

*(You can skip this step if you just pull it from [docker hub](https://hub.docker.com/r/th3j0kr/kali-ptf))*

To build simply clone the repository locally, run `docker build -t <container name> <path to build dir>`. This will start the build process which can take quite a while (1-2 hours) and creates a rather large container while the base packages are updated and the tools are installed.

## Running

To run the container without persistent storage it is as easy as running `docker run -it --rm th3j0kr/kali-ptf`. If you want to keep the files in a persistant volume you can use `docker  run -it --rm --mount src=kali-root,dst=/root --mount src=kali-postgres,dst=/var/lib/postgresql th3j0kr/kali-ptf`. This will create volumes `kali-root` and `kali-postgres` and map them to `/root` and `/var/lib/postgres` so even if the kali-ptf container is stopped and remove the data will last when it is spun back up. 

The `start_container.sh/ps1` scripts will start the container with persistent volumes shown above.

## Installed tools

- nmap
- hydra
- [RDPassSpray](https://github.com/xFreed0m/RDPassSpray)
- beef
- findsploit
- responder
- routersploit
- setoolkit
- sqlmap
- wpsploit
- dnsrecon
- enum4linux
- windows-exploit-suggester
- seclist
- pivoter
- dnscat2
- crackmapexec
- empire
- unicorn
- trevorc2
- nikto
- office365userenum
- sslscan
- wpscan
- weevely
- faraday

## Credits

Special thanks to these folks for doing much of the heavy lifting:

- [@airman604](https://medium.com/@airman604/kali-linux-in-a-docker-container-5a06311624eb)
- [Dave Kennedy](https://twitter.com/HackingDave)