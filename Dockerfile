FROM kalilinux/kali-linux-docker
LABEL version="1.2"
LABEL description="Dockerized version of Kali with the Trustedsec PTF - Penetration Testing Framework"
LABEL author="Jacob Scheid"
# Fix sources.list issue
COPY scripts/fix_sources.sh /sbin/fix_sources.sh

RUN chmod +x /sbin/fix_sources.sh
RUN /bin/bash -c /sbin/fix_sources.sh

#General updating
RUN apt-get update && \ 
    apt-get dist-upgrade -y && \
    apt-get autoremove -y && \ 
    apt-get clean -y

# Install basic tools and dependencies for tools
RUN apt-get install -y python && \ 
    apt-get install -y git && \
    apt-get install -y sudo && \ 
    apt-get install -y locate && \
    apt-get install -y vim && \
    apt-get install -y python-pexpect && \
    apt-get install -y build-essential && \
    apt-get install -y python-apt  && \
    apt-get install -y freerdp2-x11 && \
    apt-get install -y python3 && \
    apt-get install -y python3-pip && \
    apt-get install -y tmux && \
    apt-get install -y tree && \
    apt-get install -y telnet

# Install tools in repository
RUN apt-get install -y nmap && \
    apt-get install -y hydra && \
    apt-get install -y rpcbind && \
    apt-get install -y nfs-common && \
    apt-get install -y sslscan && \
    apt-get install -y whois

# Set the hostname
RUN echo "kali-docker" > /etc/hostname

# Create a pentest directory
WORKDIR /pentest

# Clone the ptf tools
RUN git clone https://github.com/trustedsec/ptf.git

# Copy the script to download specific tools
COPY /scripts/bootstrap.sh /sbin/bootstrap.sh
# Run the script to get custom tools
RUN chmod +x /sbin/bootstrap.sh
RUN bash -c /sbin/bootstrap.sh

WORKDIR /root

CMD [ "/bin/bash" ]